import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StoresModule } from './stores/stores.module';
import { CustomersModule } from './customers/customers.module';
import { EmployeesModule } from './employees/employees.module';
import { CatagorysModule } from './catagorys/catagorys.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',
      synchronize: true,
      migrations: [],
      entities: [],
    }),
    StoresModule,
    CustomersModule,
    EmployeesModule,
    CatagorysModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
